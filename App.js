import * as React from 'react';
import {Text, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeScreen from './screens/HomeScreen';
import FormScreen from './screens/FormScreen';
import BlogScreen from './screens/BlogScreen'
import Icon from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import AxiousScreen from './screens/AxiousScreen';
import ImagePickerScreen from './screens/ImagePickerScreen';

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator screenOptions={{headerShown: false}}>
        <Tab.Screen
          name="Home"
          component={HomeScreen}
          options={{
            tabBarIcon: ({color}) => (
              <Icon name="home" size={20} color="gray" />
            ),
          }}
        />
         <Tab.Screen
          name="Api"
          component={AxiousScreen}
          options={{
            tabBarIcon: ({color}) => (
              <Icon name="plus" size={20} color="gray" />
            ),
          }}
        />
        <Tab.Screen
          name="Form"
          component={FormScreen}
          options={{
            tabBarIcon: ({color}) => (
              <Icon name="form" size={20} color="gray" />
            ),
          }}
        />
         <Tab.Screen
          name="Blog"
          component={BlogScreen}
          options={{
            tabBarIcon: ({color}) => (
              <Icon2 name="blog" size={20} color="gray" />
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

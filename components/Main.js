import React from 'react';
import {
  Image,
  View,
  Button,
  StyleSheet,
  TouchableOpacity,
  Text,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

const Main = () => {
  return (
    <View>
      <Text style={styles.heading}>Explore Imani Within</Text>
      <View style={styles.container}>
        <View>
          <Image
            source={{uri: 'https://picsum.photos/200', width: 170, height: 170}}
          />
          <View style={styles.container2}>
            <Text
              style={{
                color: 'black',
                marginRight: 5,
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              Blogs
            </Text>
            <Icon
              name="rightcircleo"
              size={16}
              color="gray"
              style={{paddingTop: 5}}
            />
          </View>
        </View>
        <View>
          <Image
            source={{uri: 'https://picsum.photos/200', width: 170, height: 170}}
          />
          <View style={styles.container2}>
            <Text
              style={{
                color: 'black',
                marginRight: 5,
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              Blogs
            </Text>
            <Icon
              name="rightcircleo"
              size={16}
              color="gray"
              style={{paddingTop: 5}}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  heading: {
    fontSize: 16,
    color: 'grey',
    fontWeight: 'bold',
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    // height: 600,
    flexWrap: 'wrap',
  },
  container2: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: 10,
    flexWrap: 'wrap',
    alignItems: 'center',
  },
});

export default Main;

import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

const ViewAnalytics = () => {
  return (
    <View style={styles.container}>
      <View
        style={{
          borderBottomColor: 'gray',
          borderBottomWidth: 1,
        }}
      />
      <View style={styles.textContainer}>
        <Text style={{color: 'blue'}}>View Analytics</Text>
        <Icon name="rightcircleo" size={20} color="blue" />
      </View>
      <View
        style={{
          borderBottomColor: 'gray',
          borderBottomWidth: 1,
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 70,
    // flexDirection: 'coloumn',
    justifyContent: 'space-between',
  },
  textContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: "center"
  },
});

export default ViewAnalytics;

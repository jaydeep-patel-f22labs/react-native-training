import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import Categories from './Categories';

const HeaderText = ({heading, subHeading}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.heading}>{heading}</Text>
      <Text style={styles.subHeading}>{subHeading}</Text>
      <View style={{marginTop: 10}}>
        <Categories />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // padding: 20,
  },
  heading: {
    fontSize: 20,
    color: 'black',
    fontWeight: 'bold',
  },
  subHeading: {
    fontSize: 14,
    color: 'black',
    marginTop: 7,
  },
});

export default HeaderText;

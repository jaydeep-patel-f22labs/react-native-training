import React from 'react';
import {View, Button, StyleSheet, TouchableOpacity, Text} from 'react-native';

const Categories = () => {
  return (
    <View style={styles.maincontainer}>
      <View style={styles.container}>
        <TouchableOpacity style={styles.box}>
          <Text style={styles.text}>+ Blog</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.box}>
          <Text style={styles.text}>+ Tips & Tricks</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.box}>
          <Text style={styles.text}>+ Event</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.box}>
          <Text style={styles.text}>+ Advert</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  maincontainer: {
    height: 150,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    height: 600,
    flexWrap: 'wrap',
  },
  box: {
    width: 140,
    height: 44,
    backgroundColor: '#ADD8E6',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 16,
  },
  text: {
    fontSize: 14,
    color: 'blue',
  },
});

export default Categories;

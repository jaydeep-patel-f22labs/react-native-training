import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import Icon2 from 'react-native-vector-icons/AntDesign';
import {Formik} from 'formik';
import * as yup from 'yup';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ImagePicker from 'react-native-image-crop-picker';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const loginValidationSchema = yup.object().shape({
  email: yup
    .string()
    .email('Please enter valid email')
    .required('Email Address is Required'),
  password: yup
    .string()
    .min(8, ({min}) => `Password must be at least ${min} characters`)
    .required('Password is required'),
});

const FormScreen = () => {
  const [image, setImage] = useState();
  const [hide, setHide] = useState(false);
  const openOptions = () => {
    setHide(!hide);
  };
  const TakePhotoFormCamara = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      // console.log(image);
      setImage(image.path);
    });
  };

  const ChoosePhotoFromLibrary = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      // console.log(image);
      setImage(image.path);
    });
  };
  return (
    <KeyboardAwareScrollView>
      {/* // <SafeAreaView style={styles.container}> */}
      {/* // <StatusBar animated={true} backgroundColor="white" /> */}
      <View style={styles.container}>
        <View>
          <Icon2 name="arrowleft" size={30} color="blue" />
        </View>

        {/* header */}
        <Text style={styles.backlogo}>
          You are a few steps away from Setting up your Business Account
        </Text>

        {/* logo upload */}
        <View style={styles.imgMain}>
          <ImageBackground
            source={{
              uri: image,
            }}
            style={{height: 100, width: 100, marginTop: 20}}
            imageStyle={{borderRadius: 50, backgroundColor: 'blue'}}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={image ? {opacity: 0} : {opacity: 100}}>Logo</Text>
            </View>
          </ImageBackground>
          <TouchableOpacity
            onPress={openOptions}
            style={styles.smallimgContainer}>
            <Icon2 name="plus" size={20} color="white" />
          </TouchableOpacity>
        </View>
        <View
          style={
            hide
              ? {
                  height: 100,
                  width: 100,
                  opacity: 100,
                  marginTop: 20,
                  marginLeft: 90,
                  marginBottom: 20,
                }
              : {height: 0, width: 0, opacity: 0}
          }>
          <TouchableOpacity
            onPress={TakePhotoFormCamara}
            style={styles.button2}>
            <Text>Take photo</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={ChoosePhotoFromLibrary}
            style={styles.button2}>
            <Text>Chosse from Gallary</Text>
          </TouchableOpacity>
        </View>

        {/* form */}
        <View style={styles.form}>
          <Formik
            validationSchema={loginValidationSchema}
            initialValues={{email: '', password: ''}}
            onSubmit={values => console.log(values)}>
            {({
              handleChange,
              handleBlur,
              isValid,
              handleSubmit,
              values,
              errors,
            }) => (
              <>
                <Text style={styles.textTitle}>Email</Text>
                <TextInput
                  name="email"
                  placeholder="Email Address"
                  style={styles.textInput}
                  onChangeText={handleChange('email')}
                  onBlur={handleBlur('email')}
                  value={values.email}
                  keyboardType="email-address"
                />
                {errors.email && (
                  <Text style={{fontSize: 10, color: 'red'}}>
                    {errors.email}
                  </Text>
                )}

                <Text style={styles.textTitle}>Password</Text>
                <TextInput
                  name="password"
                  placeholder="Password"
                  style={styles.textInput}
                  onChangeText={handleChange('password')}
                  onBlur={handleBlur('password')}
                  value={values.password}
                  secureTextEntry
                />
                {errors.password && (
                  <Text style={{fontSize: 10, color: 'red'}}>
                    {errors.password}
                  </Text>
                )}
                <View style={styles.button}>
                  <Button
                    onPress={handleSubmit}
                    title="LOGIN"
                    disabled={!isValid}
                  />
                </View>
              </>
            )}
          </Formik>
        </View>
      </View>
      {/* // </SafeAreaView> */}
    </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: 'white',
  },
  backlogo: {
    paddingTop: 20,
    fontSize: 20,
  },
  imgMain: {
    alignItems: 'center',
  },
  imgContainer: {
    height: 100,
    width: 100,
    backgroundColor: 'blue',
    borderRadius: 100 / 2,
    overflow: 'hidden',
    borderWidth: 3,
    borderColor: 'blue',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  smallimgContainer: {
    height: 40,
    width: 40,
    backgroundColor: 'black',
    borderRadius: 40 / 2,
    overflow: 'hidden',
    borderWidth: 3,
    borderColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: -32,
    marginLeft: 50,
  },
  imgText: {
    fontSize: 16,
  },
  form: {
    marginTop: 40,
  },
  textTitle: {
    marginTop: 20,
  },
  textInput: {
    height: 40,
    // width: '100%',
    marginTop: 10,
    backgroundColor: 'white',
    // borderColor: 'white',
    borderWidth: StyleSheet.hairlineWidth,
    // borderRadius: 10,
    borderBottomColor: 'gray',
    borderLeftColor: 'white',
    borderRightColor: 'white',
    borderTopColor: 'white',
  },
  button: {
    padding: 40,
  },
  button2: {
    borderColor: 'black',
    backgroundColor: 'red',
    height: 50,
    width: 150,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
  },
});

export default FormScreen;

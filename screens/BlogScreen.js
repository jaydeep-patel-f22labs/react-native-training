import React from 'react';
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  Image,
  ScrollView,
} from 'react-native';

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad5e3abb28ba',
    title: 'Let your light shine and be cool always',
    name: 'jaydeep1',
    date: '22-2-2021',
    url: 'https://picsum.photos/200',
  },
  {
    id: 'bd47acbea-c1b1-46c2-aed5-3add53abb28ba2',
    title: 'Let your light shine and be cool always',
    name: 'jaydeep2',
    date: '22-2-2021',
    url: 'https://picsum.photos/200',
  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad5e3abb28ba3',
    title: 'Let your light shine and be cool always',
    name: 'jaydeep3',
    date: '22-2-2021',
    url: 'https://picsum.photos/200',
  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3add53abb28ba22',
    title: 'Let your light shine and be cool always',
    name: 'jaydeep4',
    date: '22-2-2021',
    url: 'https://picsum.photos/200',
  },
];

const Item = ({title, name, date, url}) => (
  <View style={styles.item}>
    <View>
      <Image source={{uri: `${url}`, width: 120, height: 120}} />
    </View>
    <View style={styles.text}>
      <Text style={styles.title}>{title}</Text>
      <View style={styles.text2}>
        <Text style={styles.title2}>{name}</Text>
        <Text style={styles.title3}>{date}</Text>
      </View>
    </View>
  </View>
);

const Blog = () => {
  const renderItem = ({item}) => (
    <Item title={item.title} name={item.name} date={item.date} url={item.url} />
  );

  return (
    // <ScrollView style={styles.container}>
      <View style={styles.container}>
        <FlatList
          nestedScrollEnabled
          data={DATA}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
      </View>
    // </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 30,
    alignItems: 'center',
  },
  title: {
    fontSize: 16,
  },
  title2: {
    fontSize: 14,
    color: 'blue',
  },
  title3: {
    fontSize: 14,
    color: 'black',
  },
  text: {
    width: 200,
    // alignItems: 'center',
  },
  text2: {
    marginTop: 40,
  },
});

export default Blog;

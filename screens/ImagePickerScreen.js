import React, {useState} from 'react';
import {Text, TouchableOpacity, View, StyleSheet, ImageBackground} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {black} from 'react-native-paper/lib/typescript/styles/colors';

const ImagePickerScreen = () => {
  const [image, setImage] = useState('https://api.adorable.io/avatars/80/abott@adorable.png');

  const TakePhotoFormCamara = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      console.log(image);
      setImage(image.path)
    });
  };

  const ChoosePhotoFromLibrary = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      console.log(image);
      setImage(image.path)
    });
  };

  return (
    <View>
      <TouchableOpacity onPress={TakePhotoFormCamara} style={styles.button}>
        <Text>Take photo</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={ChoosePhotoFromLibrary} style={styles.button}>
        <Text>Chosse from Gallary</Text>
      </TouchableOpacity>
      <ImageBackground
                source={{
                  uri: image,
                }}
                style={{height: 100, width: 100}}
                imageStyle={{borderRadius: 15}}>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="camera"
                    size={35}
                    color="#fff"
                    style={{
                      opacity: 0.7,
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderWidth: 1,
                      borderColor: '#fff',
                      borderRadius: 10,
                    }}
                  />
                </View>
              </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    borderColor: 'black',
    backgroundColor: 'red',
    height: 50,
    width: 150,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 100,
  },
});

export default ImagePickerScreen;

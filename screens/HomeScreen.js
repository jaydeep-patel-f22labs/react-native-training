import React from 'react';
import {Text, View, StyleSheet, StatusBar} from 'react-native';
import HeaderText from '../components/HeaderText';
import Categories from '../components/Categories';
import ViewAnalytics from '../components/ViewAnalytics';
import Main from '../components/Main';
import {Dimensions} from 'react-native';

const HomeScreen = () => {
  return (
    <View style={styles.container}>
      <StatusBar
        animated={true}
        backgroundColor="white"
        barStyle="dark-content"
      />
      <View>
        <HeaderText
          heading="Hey Random Organisation,"
          subHeading="What would you like to share today?"
        />
      </View>
      <View style={{marginTop: -10}}>
        <ViewAnalytics />
      </View>
      <View>
        <Main />
      </View>
    </View>
  );
};

const heightFull = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 80,
    height: heightFull,
  },
});

export default HomeScreen;

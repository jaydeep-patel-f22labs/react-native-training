import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ScrollView,
  Image,
} from 'react-native';
import axios from 'axios';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const Item = ({title, name, date, url}) => (
  <View style={styles.item}>
    <View>
      <Image source={{uri: `${url}`, width: 120, height: 120}} />
    </View>
    <View style={styles.text}>
      <Text style={styles.title}>{title}</Text>
      <View style={styles.text2}>
        <Text style={styles.title2}>{name}</Text>
        <Text style={styles.title3}>{date}</Text>
      </View>
    </View>
  </View>
);

const AxiousScreen = () => {
  const [state, setstate] = useState();

  useEffect(() => {
    axios
      .get('https://swapi.dev/api/films/?format=json')
      .then(function (response) {
        // console.log(response.data);
        setstate(response.data.results);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  const renderItem = ({item}) => (
    <Item
      title={item?.title}
      name={item?.director}
      date={item?.release_date}
      url="https://picsum.photos/200"
    />
  );

  return (
    <View style={styles.container}>
      <FlatList
        nestedScrollEnabled
        data={state}
        renderItem={renderItem}
        keyExtractor={item => item?.episode_id}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 30,
    alignItems: 'center',
  },
  title: {
    fontSize: 16,
  },
  title2: {
    fontSize: 14,
    color: 'blue',
  },
  title3: {
    fontSize: 14,
    color: 'black',
  },
  text: {
    width: 200,
    // alignItems: 'center',
  },
  text2: {
    marginTop: 60,
  },
});

export default AxiousScreen;
